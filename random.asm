; get a value from /dev/random

section .text:
getrandomhex:
  mov eax,5
  mov ebx,random
  mov ecx,0
  int 80h

  mov ebx,eax
  mov eax,3
  push 0 ; make stack space
  mov ecx,esp ; make stack the buffer
  mov edx,1 ; only 1 byte
  int 80h

  pop eax
  ret

getrandomnumber:
  push eax
  call getrandomhex
  pop ebx
.modloop:
  cmp eax,ebx
  jl .moddone
  sub eax,ebx
  jmp .modloop
.moddone:
  ret
  
section .data:
  random db "/dev/random",0