%include "random.asm"

section .text:
global _start
_start:
  mov al,100
  call getrandomnumber
  inc al
  mov [randnum],al
.loop:
  ; ask for & get input
  mov eax,4
  mov ebx,1
  mov ecx,ask
  mov edx,asklen
  int 80h

  mov eax,3
  mov ebx,2
  mov ecx,numstr
  mov edx,3
  int 80h

  ; convert string to number
  ; shift to right
  ; test if isn't digit
  cmp byte [numstr+2], '0'
  jge .decode1
  ; shift numstr[0:1] → numstr[1:2]
  mov al,[numstr+1]
  mov [numstr+2],al
  mov al,[numstr]
  mov [numstr+1],al
  mov byte [numstr],'0'
.decode1:
  ; test if isn't digit
  cmp byte [numstr+2], '0'
  jge .decode2
  ; shift numstr[0:1] → numstr[1:2]
  mov al,[numstr+1]
  mov [numstr+2],al
  mov al,[numstr]
  mov [numstr+1],al
  mov byte [numstr],'0'
.decode2:
  sub byte [numstr],'0'
  sub byte [numstr+1],'0'
  sub byte [numstr+2],'0'

  ; convert 3 digits to number

  mov al, [numstr+2]
  mov ah, 0
  mov [num], ax

  mov al,10
  mul byte [numstr+1]
  add [num],ax

  mov al,100
  mul byte [numstr]
  add [num],ax
  ; end of conversion
  
  mov ax,[num]
  cmp [randnum],ax
  jl .greater
  jg .less
  ; equal
  mov eax,4
  mov ebx,1
  mov ecx,correcttext
  mov edx,correctlen
  int 80h
  jmp .end
.greater:
  mov eax,4
  mov ebx,1
  mov ecx,greatertext
  mov edx,greaterlen
  int 80h
  jmp .loop ; loop
.less:
  mov eax,4
  mov ebx,1
  mov ecx,lesstext
  mov edx,lesslen
  int 80h
  jmp .loop ; loop

.end:
  mov eax,1
  mov ebx,0
  int 80h

section .data:
  ask db "Guess a number between 1-100.",0Ah
  asklen equ $ - ask
  greatertext db "Too large.",0Ah
  greaterlen equ $ - greatertext
  lesstext db "Too small.",0Ah
  lesslen equ $ - lesstext
  correcttext db "Correct!",0Ah
  correctlen equ $ - correcttext

section .bss
  numstr resb 3
  num resw 1
  randnum resb 1